# DevOps Engineer Role

Nice talking to you. Continuing with our interview process, we wanted to give you an assignment inorder to test the technical knowledge.
Here is the assignment.

### 1. Docker
Docker is a tool to create, deploy, and run applications by using containers. It allows a developer to package up an application with all of the parts it needs, such as libraries and other dependencies thus allowing him/her to `Build once Run Anywhere`.

`assignment/main.go` is an http server written in Golang. 

You have to write a [Dockerfile](https://docs.docker.com/engine/reference/builder/) for the same. 


Dockerfile should have:
- minimal base image requirement to run the go application
- multi-stage build
- Extra points if you can build from scrath i.e `FROM scratch`
- an executable to run by default when starting the container

Deliverables:
- A Dockerfile
- A README file with instructions on how to build and run the image
- Extra points if you can pass SENDER_EMAIL, PASSWORD & RECEIVER_EMAIL from enviroment variable

----

### 2. CI using Gitlab
Continuous Integration works by pushing small code chunks to your application’s codebase hosted in a Git repository, and to every push, run a pipeline of scripts to build, test, and validate the code changes before merging them into the main branch.

Setup CI to you project(Use gitlab shared runner's).

Deliverables:
- Build docker image with Gitlab CI
- Push to [GitLab Container Registry](https://docs.gitlab.com/ee/user/packages/container_registry/)
- Extra points if you tag image with CI_JOB_ID

----

### 3. Deploy above application using Kubernetes

[Kubernetes](https://kubernetes.io/docs/concepts/overview/what-is-kubernetes/) is a portable, extensible, open-source platform for managing containerized applications. 

- Write a [kubernetes deployment](https://kubernetes.io/docs/concepts/overview/working-with-objects/kubernetes-objects/) object having two replicas (pods) of the above application.
- Load balance the pods using a [service](https://kubernetes.io/docs/concepts/services-networking/service/). Expose service at port 31000.

**Tip**: Push image to [GitLab Container Registry](https://docs.gitlab.com/ee/user/packages/container_registry/) for your convenience.

Extra points if you can write a [helm chart](https://helm.sh/docs/) for the same (`values.yaml` should contain variables such as image name and version).

Deliverables:
- A file containing relevant kubernetes object
- A README file with instructions to run application on a kubernetes cluster
- For helm, submit a complete helm chart along with instructions to run on a kubernetes cluster.

----

### 4. Extracting datat from Log Files, Shell Scripts & Automated Jobs

There are three log files that are present in the logs folder. You are expected to obtain as much data out of them as possible.

- You are supposed to write a script that will do that automatically and present the output in a readable and easy to understand format.
- The output from the script should be then saved to a file.
- The file should of course follow a naming convention that it is clear to the person reviewing the logs when the log file was generated.

Once the script is completed you have to provide us with an automated way to execute your script on a daily basis.

**Note**: The script can be written in python or bash. If you are unfamiliar with them you can use a language of your liking.


# How to submit the asssigmnet 
- Fork this repo.
- Work on personal copy of this repo, and then put a merge request to orignal repo, master branch.
- Before making an merge request, make sure you have all your scripts i.e `Dockerfile, .gitlab-ci.yaml, charts folder, bash script & other things.` and please update the README.md with necessary step's to run.
- If you have any quariies please reach out to us at [Infosec](mailto:infosec@curlanalytics.com)

### You can get back to us with the assignment by the end of Monday i.e 9th Nov 2020 (Well and good if you could finish it early).  Kindly let us know if you have any questions and need some more time.